//
//  STIClient.swift
//  STITrackerKit
//
//  Created by Derek Stutsman on 5/29/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

import Foundation
import Kanna

public enum STIClient {
    // MARK: - Enum
    public enum Field: String {
        case none = ""
        case contractNumber = "Contract Number:"
        case shipFrom = "Ship From:"
        case shipTo = "Ship To:"
        case pickupDate = "Actual Pickup Date :"
        case deliveryDate = "Planned Delivery Date :"
        case shipmentStatus = "Current Shipment Status:"
        case lastLocationUpdate = "Last Location Update:"
        case deliverySigned = "Delivery Signed For By:"
    }

    public class STIError: LocalizedError {
        init(_ message: String) {
        }
    }

    // MARK: - Constants
    private static let urlTemplate = "http://extranet.stidelivers.com/BOSShipmentTracking/BOSShipTrack.aspx?ContractNumber=%@"

    private static func scrapeWebPage(_ document: HTMLDocument, completion: @escaping (Result<TrackingUpdate, Error>) -> Void) {
        // Parse it
        var field: Field = .none
        let result = TrackingUpdate()
        for tableNode in document.css("td") {
            guard let content = tableNode.content else { return }
            switch field {
            case .none:
                field = Field(rawValue: content) ?? .none
            case .contractNumber:
                result.contractNumber = content
                field = .none
            case .shipFrom:
                result.shipFrom = content
                field = .none
            case .shipTo:
                result.shipTo = content
                field = .none
            case .pickupDate:
                result.pickupDate = content
                field = .none
            case .deliveryDate:
                result.plannedDeliveryDate = content
                field = .none
            case .shipmentStatus:
                result.status = content
                field = .none
            case .lastLocationUpdate:
                result.lastLocationUpdate = content
                field = .none
            case .deliverySigned:
                result.deliverySignedForBy = content
            }
        }

        DispatchQueue.main.async {
            completion(.success(result))
        }
    }

    public static func getUpdate(for shipment: Shipment, completion: @escaping (Result<TrackingUpdate, Error>) -> Void) {
        // Assemble URL or fail
        guard let contract = shipment.contract,
            !contract.isEmpty,
            let url = URL(string: String(format: urlTemplate, contract)) else {
                return
        }

        // Do HTTP hit
        Log.debug("API: Fetching data for shipment \"\(String(describing: shipment.name))\"")
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data, error == nil,
                let document = try? HTML(html: data, url: url.absoluteString, encoding: .utf8) else {
                    DispatchQueue.main.async {
                        completion(.failure(STIError("No data!")))
                    }
                    return
            }

            // Scrape it
            scrapeWebPage(document, completion: completion)
        }
        .resume()
    }
}
