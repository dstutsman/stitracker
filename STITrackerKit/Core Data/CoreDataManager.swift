//
//  DataManager.swift
//  STITrackerKit
//
//  Created by Derek Stutsman on 5/29/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

import CoreData
import CloudKit

public class CoreDataManager {
    // MARK: - Properties
    public static let shared: CoreDataManager = CoreDataManager()
    private let identifier = "com.stutsmansoft.STITracker"
    private let modelName = "STI_Tracker"
    private let containerName = "STITracker"
    public lazy var persistentContainer: NSPersistentContainer = {
        // Find model
        let bundle = Bundle(for: CoreDataManager.self)
        guard let modelURL = bundle.url(forResource: modelName, withExtension: "momd"),
            let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
                return NSPersistentCloudKitContainer(name: containerName)
        }

        // Set up container
        let container = NSPersistentCloudKitContainer(name: containerName, managedObjectModel: managedObjectModel)
        container.loadPersistentStores(completionHandler: { storeDescription, error in
            Log.debug("COREDATA: Loaded \(storeDescription)")
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })

        // Ensure merges
        container.viewContext.automaticallyMergesChangesFromParent = true

        return container
    }()

    // MARK: - Private Debug
    private func publishSchemaToCloudkit() {
        guard let container = persistentContainer as? NSPersistentCloudKitContainer else { return }
        do {
            Log.debug("COREDATA: Initializing CloudKit Schema")
            try container.initializeCloudKitSchema()
        } catch {
            fatalError("COREDATA: Failed to init schema: \(error)")
        }
    }

    // MARK: - Init
    init() {
        Log.debug("COREDATA: Initializing data manager")

        // Initialize container, publishing schema if needed
        #if DEBUG
        if ProcessInfo.processInfo.environment["PUBLISH_SCHEMA"] != nil {
            Log.debug("COREDATA: Publishing schema")
            publishSchemaToCloudkit()
        } else {
            Log.debug("COREDATA: NOT publishing schema")
            _ = self.persistentContainer
        }
        #else
        _ = self.persistentContainer
        #endif
    }

    // MARK: - Public
    public func insert<T: NSManagedObject>() -> T {
        let context = persistentContainer.viewContext
        let result = T(context: context)
        return result
    }

    public func delete<T: NSManagedObject>(_ object: T) {
        let context = persistentContainer.viewContext
        context.delete(object)
        saveContext()
    }

    public func executeFetchRequest<T: NSManagedObject>(request: NSFetchRequest<NSFetchRequestResult>) throws -> [T] {
        let context = persistentContainer.viewContext
        let result = try context.fetch(request)
        return result as? [T] ?? []
    }

    public func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    public func refreshShipments(completion: @escaping (([Shipment]) -> Void)) {
        assert(Thread.isMainThread, "Main thread expected!")
        guard let shipments: [Shipment] = try? executeFetchRequest(request: Shipment.fetchRequest()) else {
            completion([])
            return
        }

        var changedShipments = [Shipment]()
        let group = DispatchGroup()
        group.enter()
        shipments.forEach { shipment in
            group.enter()
            // Update shipment
            let oldUpdate = shipment.lastLocationUpdate
            STIClient.getUpdate(for: shipment) {
                shipment.update(with: try? $0.get())
                if oldUpdate != shipment.lastLocationUpdate {
                    changedShipments.append(shipment)
                }
                group.leave()
            }
        }
        group.leave()

        group.notify(queue: DispatchQueue.main, execute: {
            completion(changedShipments)
        })
    }
}
