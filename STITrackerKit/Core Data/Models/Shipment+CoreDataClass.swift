//
//  Shipment+CoreDataClass.swift
//  STITrackerKit
//
//  Created by Derek Stutsman on 5/29/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//
//

import CoreData
import CoreLocation
import MapKit

public class Shipment: NSManagedObject {
    // MARK: - Constants
    let refreshInterval = 60.0

    // MARK: - Properties
    private static var pickupDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter
    }()
    private static var deliveryDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter
    }()
    public var isStale: Bool {
        guard let dateUpdated = dateUpdated, lastLocationUpdate != nil else { return true }
        return Date().timeIntervalSince(dateUpdated) > refreshInterval
    }
    public var sourceAnnotation: MKAnnotation? {
        source
    }
    public var destinationAnnotation: MKAnnotation? {
        destination
    }
    public var sortedLocations: [Location] {
        guard let locations = locations?.allObjects as? [Location] else { return [] }
        return locations.sorted {
            guard let date1 = $0.date, let date2 = $1.date else { return true }
            return date1.compare(date2) == .orderedAscending
        }
    }
    public var annotations: [MKAnnotation] {
        guard let result = locations?.allObjects as? [Location] else { return [] }
        return result.filter { $0.latitude != 0 && $0.longitude != 0 }
    }
    public var datePickupDisplayString: String? {
        guard let datePickup = datePickedUp else { return nil }
        return Shipment.pickupDateFormatter.string(from: datePickup)
    }
    public var datePlannedDisplayString: String? {
        guard let dateLow = datePlannedDeliveryLow, let dateHigh = datePlannedDeliveryHigh else { return nil }
        let dateLowString = Shipment.deliveryDateFormatter.string(from: dateLow)
        let dateHighString = Shipment.deliveryDateFormatter.string(from: dateHigh)
        return "\(dateLowString) - \(dateHighString)"
    }
    public var dateUpdatedDisplayString: String? {
        guard let date = dateUpdated else { return nil }
        return Shipment.pickupDateFormatter.string(from: date)
    }

    // MARK: - CRUD
    public static func insert(name: String, contract: String) -> Shipment {
        let result: Shipment = CoreDataManager.shared.insert()
        result.name = name
        result.contract = contract
        result.dateCreated = Date()
        result.dateUpdated = Date.distantPast
        CoreDataManager.shared.saveContext()
        return result
    }

    fileprivate func updateLocations(_ locationChanged: Bool, _ tracking: TrackingUpdate) {
        // Add location if changed
        if locationChanged, let city = tracking.lastLocationCity, let state = tracking.lastLocationState, let date = tracking.lastLocationDate {
            // Add the location object
            let location = Location.insert(city: city, state: state, date: date)
            addToLocations(location)

            // Geocode it
            if let locationString = location.displayString {
                CLGeocoder.init().geocodeAddressString(locationString) { placemark, _ in
                    assert(Thread.isMainThread)
                    if let placemark = placemark?.first,
                        let latitude = placemark.location?.coordinate.latitude,
                        let longitude = placemark.location?.coordinate.longitude {
                        location.latitude = latitude
                        location.longitude = longitude
                        CoreDataManager.shared.saveContext()
                        Log.debug("GEOCODE: Got address for \(location)")
                    }
                }
            }

            Log.debug("Shipment \(String(describing: contract)) updated, \(locations?.count ?? 0) location events")
        }
    }

    fileprivate func updateShipmentFrom(_ tracking: TrackingUpdate) {
        datePickedUp = tracking.actualPickupDate
        currentShipmentStatus = tracking.status
        lastLocationUpdate = tracking.lastLocationUpdate
        datePlannedDeliveryLow = tracking.deliveryDateLow
        datePlannedDeliveryHigh = tracking.deliveryDateHigh
        shipFromCity = tracking.pickupCity
        shipFromState = tracking.pickupState
        shipFromZip = tracking.pickupZip
        shipToCity = tracking.deliveryCity
        shipToState = tracking.deliveryState
        shipToZip = tracking.deliveryZip
        signedForBy = tracking.deliverySignedForBy
    }

    public func update(with tracking: TrackingUpdate?) {
        assert(Thread.isMainThread, "Main thread expected!")
        // Bail if tracking is bogus
        guard let tracking = tracking, tracking.status != nil else { return }

        // Update datestamp
        dateUpdated = Date()

        // Check for changes
        let locationChanged = tracking.lastLocationUpdate != lastLocationUpdate

        // Update shipment properties
        updateShipmentFrom(tracking)

        // Update the waypoitns
        updateLocations(locationChanged, tracking)

        //TODO REMOVE
        source = nil
        destination = nil

        // Update source?
        if source == nil, let shipFrom = tracking.shipFrom, let city = tracking.pickupCity, let state = tracking.pickupState, let pickupDate = tracking.actualPickupDate {
            // Find or create location
            if let existingLocation = Location.find(city: city, state: state) {
                // Reuse existing location
                source = existingLocation
            } else {
                // Create new one
                CLGeocoder.init().geocodeAddressString(shipFrom) { placemark, _ in
                    if let placemark = placemark?.first,
                        let latitude = placemark.location?.coordinate.latitude,
                        let longitude = placemark.location?.coordinate.longitude {
                        self.source = Location.insert(city: city, state: state, date: pickupDate)
                        self.source?.latitude = latitude
                        self.source?.longitude = longitude
                    }
                }
            }
        }

        // Update destination?
        if destination == nil, let shipTo = tracking.shipTo, let city = tracking.deliveryCity, let state = tracking.deliveryState {
            // Find or create location
            if let existingLocation = Location.find(city: city, state: state) {
                // Reuse existing location
                destination = existingLocation
            } else {
                // Create new one
                CLGeocoder.init().geocodeAddressString(shipTo) { placemark, _ in
                    if let placemark = placemark?.first,
                        let latitude = placemark.location?.coordinate.latitude,
                        let longitude = placemark.location?.coordinate.longitude {
                        self.destination = Location.insert(city: city, state: state, date: Date())
                        self.destination?.latitude = latitude
                        self.destination?.longitude = longitude
                    }
                }
            }
        }

        // Save
        Log.debug("Shipment \(String(describing: contract)) updated, \(locations?.count ?? 0) location events")
        CoreDataManager.shared.saveContext()
    }

    // MARK: - Fetch Requests
    public class func sortedFetchRequest() -> NSFetchRequest<NSFetchRequestResult> {
        let result = NSFetchRequest<NSFetchRequestResult>(entityName: "Shipment")
        result.sortDescriptors = [NSSortDescriptor(key: "dateCreated", ascending: false)]
        return result
    }
}
