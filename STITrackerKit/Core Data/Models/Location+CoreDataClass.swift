//
//  Location+CoreDataClass.swift
//  STITrackerKit
//
//  Created by Derek Stutsman on 5/29/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//
//

import CoreData
import CoreLocation
import Foundation
import MapKit

public class Location: NSManagedObject {
    // MARK: - Properties
    public var displayString: String? {
        guard let city = city, let state = state else { return nil }
        return "\(city), \(state)"
    }

    public var isLatest: Bool {
        guard let shipment = shipment,
            let latest = shipment.sortedLocations.last(where: { $0.latitude != 0 }) else { return false }
        return latest == self
    }

    // MARK: - CRUD
    public static func insert(city: String, state: String, date: Date) -> Location {
        let result: Location = CoreDataManager.shared.insert()
        result.city = city
        result.state = state
        result.date = date
        result.dateCreated = Date()
        return result
    }

    public static func find(city: String, state: String) -> Location? {
        let request: NSFetchRequest<NSFetchRequestResult> = Location.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "dateCreated", ascending: false)]
        request.predicate = NSPredicate(format: "city == %@ AND state == %@", city, state)
        return try? CoreDataManager.shared.executeFetchRequest(request: request).first
    }
}

extension Location: MKAnnotation {
    public static var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter
    }()
    public var coordinate: CLLocationCoordinate2D { CLLocationCoordinate2D(latitude: latitude, longitude: longitude ) }
    public var title: String? { displayString }
    public var subtitle: String? {
        // Source?
        if let sources = sources, !sources.allObjects.isEmpty {
            let strings: [String] = sources.allObjects.map {
                guard let shipment = $0 as? Shipment, let name = shipment.name, let pickup = shipment.datePickupDisplayString else { return "" }
                return "\(name): \(pickup)"
            }
            return strings.joined(separator: "\n")
        }

        // Destination?
        if let destinations = destinations, !destinations.allObjects.isEmpty {
            let strings: [String] = destinations.allObjects.map {
                guard let shipment = $0 as? Shipment, let name = shipment.name, let pickup = shipment.datePlannedDisplayString else { return "" }
                return "\(name): \(pickup)"
            }
            return strings.joined(separator: "\n")
        }

        guard let date = date else { return nil }
        return Location.dateFormatter.string(from: date)
    }
}
