//
//  TrackingUpdate.swift
//  STITrackerKit
//
//  Created by Derek Stutsman on 5/29/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

import Foundation

public class TrackingUpdate {
    // MARK: - Properties
    public var contractNumber: String?
    public var shipFrom: String?
    public var shipTo: String?
    public var pickupDate: String?
    public var plannedDeliveryDate: String?
    public var status: String?
    public var lastLocationUpdate: String?
    public var deliverySignedForBy: String?

    //ATLANTA, GA On: 05/29 At: 22:33
    public var lastLocationCity: String? {
        lastLocationUpdate?.components(separatedBy: ":").first?.components(separatedBy: ", ").first
    }

    public var lastLocationState: String? {
        lastLocationUpdate?.components(separatedBy: ":").first?.components(separatedBy: " ")[1]
    }

    public var lastLocationDate: Date? {
        guard let dateString = lastLocationUpdate?.components(separatedBy: ":")[1].components(separatedBy: .whitespaces)[1],
            let monthString = dateString.components(separatedBy: "/").first,
            let dayString = dateString.components(separatedBy: "/").last,
            let timeString = lastLocationUpdate?.components(separatedBy: "At: ").last,
            let hourString = timeString.components(separatedBy: ":").first,
            let minString = timeString.components(separatedBy: ":").last else {
                return nil
        }
        // TODO: FIX!
        let year = (monthString == "12") ? 2020 : 2021
        let dateComps = DateComponents(calendar: Calendar.current, year: year, month: Int(monthString), day: Int(dayString), hour: Int(hourString), minute: Int(minString))
        return dateComps.date
    }

    public var actualPickupDate: Date? {
        guard let dateString = pickupDate?.components(separatedBy: .whitespaces).first,
            let monthString = dateString.components(separatedBy: "/").first,
            let dayString = dateString.components(separatedBy: "/").last,
            let timeString = pickupDate?.components(separatedBy: .whitespaces).last,
            let hourString = timeString.components(separatedBy: ":").first,
            let minString = timeString.components(separatedBy: ":").last else {
                return nil
        }
        // TODO: FIX!
        let year = (monthString == "12") ? 2020 : 2021
        let dateComps = DateComponents(calendar: Calendar.current, year: year, month: Int(monthString), day: Int(dayString), hour: Int(hourString), minute: Int(minString))
        return dateComps.date
    }

    // FRANKLIN, TN 37064
    public var pickupCity: String? {
        shipFrom?.components(separatedBy: ",").first
    }

    public var pickupState: String? {
        shipFrom?.components(separatedBy: ",").last?.components(separatedBy: .whitespaces)[1].trimmingCharacters(in: .whitespaces)
    }

    public var pickupZip: String? {
        shipFrom?.components(separatedBy: ",").last?.components(separatedBy: .whitespaces).last?.trimmingCharacters(in: .whitespaces)
    }

    public var deliveryCity: String? {
        shipTo?.components(separatedBy: ",").first
    }

    public var deliveryState: String? {
        shipTo?.components(separatedBy: ",").last?.components(separatedBy: .whitespaces)[1].trimmingCharacters(in: .whitespaces)
    }

    public var deliveryZip: String? {
        shipTo?.components(separatedBy: ",").last?.components(separatedBy: .whitespaces).last?.trimmingCharacters(in: .whitespaces)
    }

    // 06/05 To 06/08
    public var deliveryDateLow: Date? {
        guard let dateString = plannedDeliveryDate?.components(separatedBy: " To ").first,
            let monthString = dateString.components(separatedBy: "/").first,
            let dayString = dateString.components(separatedBy: "/").last,
            let month = Int(monthString) else { return nil }
        // TODO: FIX
        let year = (month == 12) ? 2020 : 2021
        let dateComps = DateComponents(year: year, month: Int(monthString), day: Int(dayString))
        return Calendar.current.date(from: dateComps)
    }

    public var deliveryDateHigh: Date? {
        guard let dateString = plannedDeliveryDate?.components(separatedBy: " To ").last,
            let monthString = dateString.components(separatedBy: "/").first,
            let dayString = dateString.components(separatedBy: "/").last,
            let month = Int(monthString) else { return nil }
        // TODO: FIX
        let year = (month == 12) ? 2020 : 2021
        let dateComps = DateComponents(year: year, month: Int(monthString), day: Int(dayString))
        return Calendar.current.date(from: dateComps)
    }
}
