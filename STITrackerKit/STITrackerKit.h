//
//  STITrackerKit.h
//  STITrackerKit
//
//  Created by Derek Stutsman on 5/29/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for STITrackerKit.
FOUNDATION_EXPORT double STITrackerKitVersionNumber;

//! Project version string for STITrackerKit.
FOUNDATION_EXPORT const unsigned char STITrackerKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <STITrackerKit/PublicHeader.h>


