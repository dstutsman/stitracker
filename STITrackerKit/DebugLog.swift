//
//  DebugLog.swift
//  STITrackerKit
//
//  Created by Derek Stutsman on 5/29/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

import Foundation

public enum Log {
    public static func debug(_ message: String) {
        #if DEBUG
        NSLog(message)
        #endif
    }
}
