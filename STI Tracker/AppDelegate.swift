//
//  AppDelegate.swift
//  STI Tracker
//
//  Created by Derek Stutsman on 5/29/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

import BackgroundTasks
import CoreData
import STITrackerKit
import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let backgroundTaskID = "com.stutsmansoft.stiupdate"
    let backgroundInterval = 60.0 * 20.0

    //swiftlint:disable discouraged_optional_collection
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Register the background task
        BGTaskScheduler.shared.register(forTaskWithIdentifier: backgroundTaskID, using: DispatchQueue.main) { task in
            self.handleAppRefreshTask(task)
        }

        // Register local notifications
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { _, _ in
        }

        return true
    }
    //swiftlint:enable discouraged_optional_collection

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Background Tasks
    func scheduleRefreshTask() {
        let task = BGAppRefreshTaskRequest(identifier: backgroundTaskID)
        task.earliestBeginDate = Date(timeIntervalSinceNow: backgroundInterval)
        do {
            Log.debug("BGREFRESH: Scheduling bg task")
            try BGTaskScheduler.shared.submit(task)
            Log.debug("BGREFRESH: Scheduled bg task")
        } catch {
            Log.debug("BGREFRESH: Error scheduling task: \(error)")
        }
    }

    func scheduleLocalNotificationForShipment(_ shipment: Shipment) {
        guard let name = shipment.name, let lastUpdate = shipment.lastLocationUpdate else { return }

        // Set up notification message
        let content = UNMutableNotificationContent()
        content.title = "Shipment \"\(name)\" updated!"
        content.body = lastUpdate

        // Set up the trigger
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)

        // Create the request
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)

        // Schedule the request with the system.
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.add(request) { error in
            if error != nil {
                Log.debug("NOTIFICATION: Error \(String(describing: error))")
            }
        }
    }

    func handleAppRefreshTask(_ task: BGTask) {
        Log.debug("BGREFRESH: Entered Task Handler")

        // Hook timeout
        task.expirationHandler = {
            Log.debug("BGREFRESH: Timed out!")
            task.setTaskCompleted(success: false)
        }

        Log.debug("BGREFRESH: Checking for new data")
        CoreDataManager.shared.refreshShipments {changedShipments in
            // Log result
            if !changedShipments.isEmpty {
                Log.debug("BGREFRESH: Had new data \(changedShipments.count) shipments!")

                // Notify changes
                changedShipments.forEach {
                    self.scheduleLocalNotificationForShipment($0)
                }

            } else {
                Log.debug("BGREFRESH: No new data!")
            }

            // Report done
            task.setTaskCompleted(success: true)
        }

        // Schedule another refresh task
        self.scheduleRefreshTask()
    }
}
