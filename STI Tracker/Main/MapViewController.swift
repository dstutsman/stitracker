//
//  MapViewController.swift
//  STI Tracker
//
//  Created by Derek Stutsman on 5/29/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

import CoreData
import MapKit
import STITrackerKit
import UIKit

class MapViewController: UIViewController {
    // MARK: - Properties
    @IBOutlet private var mapView: MKMapView!
    private var shipmentController: NSFetchedResultsController<NSFetchRequestResult>?
    private var directionRequests = [MKDirections.Request]()

    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        // Wire up results controller
        shipmentController = NSFetchedResultsController(fetchRequest: Shipment.sortedFetchRequest(), managedObjectContext: CoreDataManager.shared.persistentContainer.viewContext, sectionNameKeyPath: "contract", cacheName: "shipments")
        shipmentController?.delegate = self

        // Do initial fetch
        try? shipmentController?.performFetch()

        // Force an update
        //checkForUpdates()

        // Force a refresh when tasking back in
        NotificationCenter.default.addObserver(self, selector: #selector(refreshClicked(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Pass shipment
        if let destination = segue.destination as? ShipmentDetailsViewController, let shipments = sender as? [Shipment] {
            destination.shipments = shipments
        }
    }

    // MARK: - Data I/O
    func checkForUpdates() {
        CoreDataManager.shared.refreshShipments(completion: { _ in
        })
    }

    // MARK: - UI Updates
    func updateMapView() {
        guard directionRequests.isEmpty, let shipments = shipmentController?.fetchedObjects as? [Shipment], !shipments.isEmpty else { return }

        // Remove all annotations and overlays
        Log.debug("MAP: Clearing routes")
        self.mapView.removeAnnotations(mapView.annotations)
        self.mapView.removeOverlays(mapView.overlays)

        // Loop all shipments and update
        shipments.forEach { shipment in
            // Add source and destination pins
            Log.debug("MAP: Adding start/end")
            let baseAnnotations: [MKAnnotation] = [shipment.sourceAnnotation, shipment.destinationAnnotation].compactMap { $0 }
            self.mapView.addAnnotations(baseAnnotations)

            // Add annotations for the location stops
            Log.debug("MAP: Adding waypoints")
            self.mapView.addAnnotations(shipment.annotations)

            // Plot routes
            self.plotRoutes(for: shipment)
        }

        // Zoom to fit everything
        Log.debug("MAP: Zooming to fit")
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
    }

    func plotRoutes(for shipment: Shipment) {
        Log.debug("MAP: Shipment \(String(describing: shipment.name)) has \(String(describing: shipment.locations?.count)) waypoints")
        guard let source = shipment.source else { return }

        // Draw lines for all the locations
        var lastLocation = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: source.latitude, longitude: source.longitude))
        shipment.sortedLocations.filter { $0.latitude != 0 }.forEach { location in
            // Create placemark
            let placemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))

            // Make a directions request
            let request = MKDirections.Request()
            request.source = MKMapItem(placemark: lastLocation)
            request.destination = MKMapItem(placemark: placemark)
            request.transportType = .automobile
            request.requestsAlternateRoutes = false
            directionRequests.append(request)
            MKDirections(request: request).calculate { response, _ in
                Log.debug("MAP: Shipment \(String(describing: shipment.name)) got route")
                if let response = response, let route = response.routes.first, self.directionRequests.contains(request) {
                    self.mapView.addOverlay(route.polyline)
                }
                if let index = self.directionRequests.firstIndex(of: request) {
                    self.directionRequests.remove(at: index)
                }
            }

            // Set new source
            lastLocation = placemark
        }
    }

    // MARK: - IBActions
    @IBAction private func refreshClicked(_ sender: AnyObject?) {
        guard Thread.isMainThread else {
            DispatchQueue.main.async {
                self.refreshClicked(sender)
            }
            return
        }
        checkForUpdates()
    }
}

// MARK: - MKMapViewDelegate
extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Use pins for start and stop
        if let startStop = annotation as? Location, (startStop.sources?.count ?? 0 > 0 || startStop.destinations?.count ?? 0 > 0) {
            let result = MKPinAnnotationView(annotation: startStop, reuseIdentifier: "pin")
            result.canShowCallout = true
            result.displayPriority = .required
            result.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            let label = UILabel()
            label.font = UIFont.preferredFont(forTextStyle: .caption1)
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
            label.text = annotation.subtitle ?? ""
            result.detailCalloutAccessoryView = label
            return result
        }

        // Ignore anything else but locations
        guard let location = annotation as? Location else { return nil }

        // Use truck for the latest
        if location.isLatest {
            let result = MKAnnotationView(annotation: location, reuseIdentifier: "truck")
            result.image = UIImage(named: "truck")
            result.canShowCallout = true
            result.displayPriority = .required
            let label = UILabel()
            label.font = UIFont.preferredFont(forTextStyle: .caption1)
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
            if let name = location.shipment?.name, let date = location.date {
                let dateString = Location.dateFormatter.string(from: date)
                label.text = "\(name): \(dateString)"
            }
            result.detailCalloutAccessoryView = label
            return result
        } else {
            let result = MKAnnotationView(annotation: location, reuseIdentifier: "reddot")
            result.image = UIImage(named: "reddot")
            result.canShowCallout = true
            result.displayPriority = .required
            return result
        }
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = UIColor.blue
        return polylineRenderer
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let location = view.annotation as? Location else { return }
        var shipments = [Shipment]()
        location.sources?.forEach {
            if let shipment = $0 as? Shipment {
                shipments.append(shipment)
            }
        }
        location.destinations?.forEach {
            if let shipment = $0 as? Shipment {
                shipments.append(shipment)
            }
        }
        self.performSegue(withIdentifier: "shipmentDetailsSegue", sender: shipments)
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension MapViewController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        updateMapView()
    }
}
