//
//  ShipmentDetailsViewController.swift
//  STI Tracker
//
//  Created by Derek Stutsman on 5/30/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

import STITrackerKit
import UIKit

class ShipmentDetailsViewController: UIViewController {
    // MARK: - Enums
    enum Fields: Int, CaseIterable {
        case contractNumber
        case shipFrom
        case shipTo
        case actualPickupDate
        case plannedDeliveryDate
        case status
        case lastLocationUpdate
        case signedBy
        case deleteButton
    }

    // MARK: - Properties
    @IBOutlet private var contractNumberLabel: UILabel!
    @IBOutlet private var shipFromLabel: UILabel!
    @IBOutlet private var shipToLabel: UILabel!
    @IBOutlet private var actualPickupDateLabel: UILabel!
    @IBOutlet private var plannedDeliveryDateLabel: UILabel!
    @IBOutlet private var statusLabel: UILabel!
    @IBOutlet private var lastUpdateLabel: UILabel!
    @IBOutlet private var signedByLabel: UILabel!
    public var shipments = [Shipment]()

    // MARK: - Methods
    func deleteShipment(_ shipment: Shipment) {
        guard let name = shipment.name else { return }

        // Set up alert
        let alertController = UIAlertController(title: "Delete Shipment", message: "Are you sure you want to delete shipment \"\(name)?\"", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
            CoreDataManager.shared.delete(shipment)
            self.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension ShipmentDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        shipments.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        shipments[section].name
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Fields.allCases.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let field = Fields(rawValue: indexPath.item) else { return UITableViewCell() }
        let shipment = shipments[indexPath.section]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DataCell", for: indexPath)

        switch field {
        case .contractNumber:
            cell.textLabel?.text = "Contract Number:"
            cell.detailTextLabel?.text = shipment.contract

        case .shipFrom:
            cell.textLabel?.text = "Ship From:"
            if let state = shipment.shipFromState, let city = shipment.shipFromCity, let zip = shipment.shipFromZip {
                cell.detailTextLabel?.text = "\(city), \(state) \(zip)"
            }

        case .actualPickupDate:
            cell.textLabel?.text = "Actual Pickup Date:"
            cell.detailTextLabel?.text = shipment.datePickupDisplayString

        case .shipTo:
            cell.textLabel?.text = "Ship To:"
            if let state = shipment.shipToState, let city = shipment.shipToCity, let zip = shipment.shipToZip {
                cell.detailTextLabel?.text = "\(city), \(state) \(zip)"
            }

        case .plannedDeliveryDate:
            cell.textLabel?.text = "Planned Delivery Date:"
            cell.detailTextLabel?.text = shipment.datePlannedDisplayString

        case .status:
            cell.textLabel?.text = "Current Shipment Status:"
            cell.detailTextLabel?.text = shipment.currentShipmentStatus

        case .lastLocationUpdate:
            cell.textLabel?.text = "Last Location Update:"
            cell.detailTextLabel?.text = shipment.lastLocationUpdate

        case .signedBy:
            cell.textLabel?.text = "Signed By:"
            cell.detailTextLabel?.text = shipment.signedForBy

        case .deleteButton:
            if let deleteCell = tableView.dequeueReusableCell(withIdentifier: "DeleteButtonCell", for: indexPath) as? DeleteButtonTableViewCell {
                deleteCell.deleteBlock = {
                    self.deleteShipment(shipment)
                }
                return deleteCell
            }
        }

        return cell
    }
}
