//
//  AddShipmentViewController.swift
//  STI Tracker
//
//  Created by Derek Stutsman on 5/30/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

import STITrackerKit
import UIKit

class AddShipmentViewController: UIViewController {
    // MARK: - Properties
    @IBOutlet private var contractTextField: UITextField!
    @IBOutlet private var nameTextField: UITextField!
    @IBOutlet private var doneButton: UIBarButtonItem!

    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        contractTextField.becomeFirstResponder()
        updateUI()
    }

    // MARK: - UI Updates
    private func updateUI() {
        guard let contract = contractTextField.text, let name = nameTextField.text else { return }
        doneButton.isEnabled = !contract.isEmpty && !name.isEmpty
    }

    // MARK: - IBActions
    @IBAction private func cancelClicked(_ sender: AnyObject?) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction private func doneClicked(_ sender: AnyObject?) {
        guard let contract = contractTextField.text, let name = nameTextField.text else { return }
        // Insert new shipment, refresh it, and exit
        let shipment = Shipment.insert(name: name, contract: contract)
        STIClient.getUpdate(for: shipment) { trackingUpdate in
            try? shipment.update(with: trackingUpdate.get())
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension AddShipmentViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        updateUI()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        updateUI()
    }
}
