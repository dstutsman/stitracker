//
//  DeleteButtonTableViewCell.swift
//  STI Tracker
//
//  Created by Derek Stutsman on 6/4/20.
//  Copyright © 2020 Stutsman Software, LLC. All rights reserved.
//

import UIKit

class DeleteButtonTableViewCell: UITableViewCell {
    // MARK: - Properties
    var deleteBlock: (() -> Void)?

    // MARK: - IBActions
    @IBAction private func deleteClicked() {
        deleteBlock?()
    }
}
